// enables intelligent code completion for Cypress commands
// https://on.cypress.io/intelligent-code-completion
/// <reference types="cypress" />
describe('Spec A', () => {
  it('works', () => {
    cy.wrap('Hello').wait(100).should('equal', 'Hello')
  })
})
